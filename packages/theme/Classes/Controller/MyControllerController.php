<?php

class Tx_Rsysworkbook_Controller_ExportController extends Tx_Extbase_MVC_Controller_ActionController
{
    // ------------------------------------------------------------
    // properties
    // ------------------------------------------------------------

    protected $settings = [];

    /**
     * frontendUserRepository.
     *
     * @var Tx_Rsysworkbook_Domain_Repository_FrontendUserRepository
     */
    protected $frontendUserRepository;

    /**
     * action list.
     */
    public function indexAction(): void
    {
        // just renders template
    }

    /**
     * action export.
     */
    public function exportAction(): void
    {
        // do all the export stuff here...

        // ----------------------------------------
        // download csv file
        // ----------------------------------------
        // prepare infos
        $cType = 'application/csv';
        $fileLen = filesize($exportFileName);
        $headers = [
            'Pragma' => 'public',
            'Expires' => 0,
            'Cache-Control' => 'must-revalidate, post-check=0, pre-check=0',
            'Cache-Control' => 'public',
            'Content-Description' => 'File Transfer',
            'Content-Type' => $cType,
            'Content-Disposition' => 'attachment; filename="' . $fname . '"',
            'Content-Transfer-Encoding' => 'binary',
            'Content-Length' => $fileLen,
        ];

        // send headers
        foreach ($headers as $header => $data) {
            $this->response->setHeader($header, $data);
        }

        $this->response->sendHeaders();

        // send file
        @readfile($exportFileName);
        exit;
    }

    protected function initializeAction(): void
    {
        $this->frontendUserRepository = t3lib_div::makeInstance('Tx_Rsysworkbook_Domain_Repository_FrontendUserRepository');

        $configurationManager = t3lib_div::makeInstance('Tx_Extbase_Configuration_BackendConfigurationManager');
        $this->settings = $configurationManager->getConfiguration(
            $this->request->getControllerExtensionName(),
            $this->request->getPluginName()
        );
        $this->settings = $this->settings['settings'];
    }
}

<?php

namespace CS\Theme\Domain\Model;

/***
 *
 * This file is part of the "Generalinformation" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2018 Sven Harders <typo3@svenharders.de>, s;harders
 *
 ***/

/**
 * Generalinformation.
 */
class Generalinformation extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity
{
    /**
     * companyname.
     *
     * @var string
     */
    protected $companyname = '';

    /**
     * logo.
     *
     * @var \TYPO3\CMS\Extbase\Domain\Model\FileReference
     * @TYPO3\CMS\Extbase\Annotation\ORM\Cascade remove
     */
    protected $logo = null;

    /**
     * address.
     *
     * @var string
     */
    protected $address = '';

    /**
     * zip.
     *
     * @var string
     */
    protected $zip = '';

    /**
     * city.
     *
     * @var string
     */
    protected $city = '';

    /**
     * email.
     *
     * @var string
     */
    protected $email = '';

    /**
     * phone.
     *
     * @var string
     */
    protected $phone = '';

    /**
     * opentimes.
     *
     * @var string
     */
    protected $opentimes = '';

    /**
     * fax.
     *
     * @var string
     */
    protected $fax = '';

    /**
     * registergericht.
     *
     * @var string
     */
    protected $registrationcourt = '';

    /**
     * registernummer.
     *
     * @var string
     */
    protected $registrationnumber = '';

    /**
     * CEO.
     *
     * @var string
     */
    protected $ceo = '';

    /**
     * Umsatzsteuer ID.
     *
     * @var string
     */
    protected $taxid = '';

    /**
     * fields.
     *
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\CS\Theme\Domain\Model\Field>
     * @TYPO3\CMS\Extbase\Annotation\ORM\Cascade remove
     */
    protected $fields = null;

    /**
     * socialmedias.
     *
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\CS\Theme\Domain\Model\Socialmedia>
     * @TYPO3\CMS\Extbase\Annotation\ORM\Cascade remove
     */
    protected $socialmedias = null;

    /**
     * __construct.
     */
    public function __construct()
    {
        //Do not remove the next line: It would break the functionality
        $this->initStorageObjects();
    }

    /**
     * Returns the companyname.
     *
     * @return string
     */
    public function getCompanyname()
    {
        return $this->companyname;
    }

    /**
     * Sets the companyname.
     *
     * @param string $companyname
     */
    public function setCompanyname($companyname): void
    {
        $this->companyname = $companyname;
    }

    /**
     * Returns the logo.
     *
     * @return \TYPO3\CMS\Extbase\Domain\Model\FileReference
     */
    public function getLogo()
    {
        return $this->logo;
    }

    /**
     * Sets the logo.
     */
    public function setLogo(\TYPO3\CMS\Extbase\Domain\Model\FileReference $logo): void
    {
        $this->logo = $logo;
    }

    /**
     * Returns the address.
     *
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Sets the address.
     *
     * @param string $address
     */
    public function setAddress($address): void
    {
        $this->address = $address;
    }

    /**
     * Returns the zip.
     *
     * @return string
     */
    public function getZip()
    {
        return $this->zip;
    }

    /**
     * Sets the zip.
     *
     * @param string $zip
     */
    public function setZip($zip): void
    {
        $this->zip = $zip;
    }

    /**
     * Returns the city.
     *
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Sets the city.
     *
     * @param string $city
     */
    public function setCity($city): void
    {
        $this->city = $city;
    }

    /**
     * Returns the email.
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Sets the email.
     *
     * @param string $email
     */
    public function setEmail($email): void
    {
        $this->email = $email;
    }

    /**
     * Returns the phone.
     *
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Sets the phone.
     *
     * @param string $phone
     */
    public function setPhone($phone): void
    {
        $this->phone = $phone;
    }

    /**
     * Returns the opentimes.
     *
     * @return string
     */
    public function getOpentimes()
    {
        return $this->opentimes;
    }

    /**
     * Sets the opentimes.
     *
     * @param string $opentimes
     */
    public function setOpentimes($opentimes): void
    {
        $this->opentimes = $opentimes;
    }

    /**
     * Returns the fax.
     *
     * @return string
     */
    public function getFax()
    {
        return $this->fax;
    }

    /**
     * Sets the fax.
     *
     * @param string $fax
     */
    public function setFax($fax): void
    {
        $this->fax = $fax;
    }

    /**
     * Returns the registrationcourt.
     *
     * @return string
     */
    public function getRegistrationcourt()
    {
        return $this->registrationcourt;
    }

    /**
     * Sets the registrationcourt.
     *
     * @param string $registrationcourt
     */
    public function setRegistrationcourt($registrationcourt): void
    {
        $this->registrationcourt = $registrationcourt;
    }

    /**
     * Returns the registrationnumber.
     *
     * @return string
     */
    public function getRegistrationnumber()
    {
        return $this->registrationnumber;
    }

    /**
     * Sets the registrationnumber.
     *
     * @param string $registrationnumber
     */
    public function setRegistrationnumber($registrationnumber): void
    {
        $this->registrationnumber = $registrationnumber;
    }

    /**
     * Returns the ceo.
     *
     * @return string
     */
    public function getCeo()
    {
        return $this->ceo;
    }

    /**
     * Sets the ceo.
     *
     * @param string $ceo
     */
    public function setCeo($ceo): void
    {
        $this->ceo = $ceo;
    }

    /**
     * Returns the taxid.
     *
     * @return string
     */
    public function getTaxid()
    {
        return $this->taxid;
    }

    /**
     * Sets the taxid.
     *
     * @param string $taxid
     */
    public function setTaxid($taxid): void
    {
        $this->taxid = $taxid;
    }

    /**
     * Adds a Field.
     */
    public function addField(\CS\Theme\Domain\Model\Field $field): void
    {
        $this->fields->attach($field);
    }

    /**
     * Removes a Field.
     *
     * @param \CS\Theme\Domain\Model\Field $fieldToRemove The Field to be removed
     */
    public function removeField(\CS\Theme\Domain\Model\Field $fieldToRemove): void
    {
        $this->fields->detach($fieldToRemove);
    }

    /**
     * Returns the fields.
     *
     * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\CS\Theme\Domain\Model\Field> $fields
     */
    public function getFields()
    {
        return $this->fields;
    }

    /**
     * Sets the fields.
     *
     * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\CS\Theme\Domain\Model\Field> $fields
     */
    public function setFields(\TYPO3\CMS\Extbase\Persistence\ObjectStorage $fields): void
    {
        $this->fields = $fields;
    }

    /**
     * Adds a Socialmedia.
     */
    public function addSocialmedia(\CS\Theme\Domain\Model\Socialmedia $socialmedia): void
    {
        $this->socialmedias->attach($socialmedia);
    }

    /**
     * Removes a Socialmedia.
     *
     * @param \CS\Theme\Domain\Model\Socialmedia $socialmediaToRemove The Socialmedia to be removed
     */
    public function removeSocialmedia(\CS\Theme\Domain\Model\Socialmedia $socialmediaToRemove): void
    {
        $this->socialmedias->detach($socialmediaToRemove);
    }

    /**
     * Returns the socialmedias.
     *
     * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\CS\Theme\Domain\Model\Socialmedia> $socialmedias
     */
    public function getSocialmedias()
    {
        return $this->socialmedias;
    }

    /**
     * Sets the socialmedias.
     *
     * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\CS\Theme\Domain\Model\Socialmedia> $socialmedias
     */
    public function setSocialmedias(\TYPO3\CMS\Extbase\Persistence\ObjectStorage $socialmedias): void
    {
        $this->socialmedias = $socialmedias;
    }

    /**
     * Initializes all ObjectStorage properties
     * Do not modify this method!
     * It will be rewritten on each save in the extension builder
     * You may modify the constructor of this class instead.
     */
    protected function initStorageObjects(): void
    {
        $this->fields = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
        $this->socialmedias = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
    }
}

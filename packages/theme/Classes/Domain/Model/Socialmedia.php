<?php

namespace CS\Theme\Domain\Model;

/***
 *
 * This file is part of the "Generalinformation" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2018 Sven Harders <typo3@svenharders.de>, s;harders
 *
 ***/

/**
 * Socialmedia.
 */
class Socialmedia extends \TYPO3\CMS\Extbase\DomainObject\AbstractValueObject
{
    /**
     * title.
     *
     * @var string
     */
    protected $title = '';

    /**
     * username.
     *
     * @var string
     */
    protected $username = '';

    /**
     * profileurl.
     *
     * @var string
     */
    protected $profileurl = '';

    /**
     * profilepicture.
     *
     * @var \TYPO3\CMS\Extbase\Domain\Model\FileReference
     * @TYPO3\CMS\Extbase\Annotation\ORM\Cascade remove
     */
    protected $profilepicture = null;

    /**
     * Returns the title.
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Sets the title.
     *
     * @param string $title
     */
    public function setTitle($title): void
    {
        $this->title = $title;
    }

    /**
     * Returns the username.
     *
     * @return string
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * Sets the username.
     *
     * @param string $username
     */
    public function setUsername($username): void
    {
        $this->username = $username;
    }

    /**
     * Returns the profileurl.
     *
     * @return string
     */
    public function getProfileurl()
    {
        return $this->profileurl;
    }

    /**
     * Sets the profileurl.
     *
     * @param string $profileurl
     */
    public function setProfileurl($profileurl): void
    {
        $this->profileurl = $profileurl;
    }

    /**
     * Returns the profilepicture.
     *
     * @return \TYPO3\CMS\Extbase\Domain\Model\FileReference
     */
    public function getProfilepicture()
    {
        return $this->profilepicture;
    }

    /**
     * Sets the profilepicture.
     */
    public function setProfilepicture(\TYPO3\CMS\Extbase\Domain\Model\FileReference $profilepicture): void
    {
        $this->profilepicture = $profilepicture;
    }
}

<?php

namespace CS\Theme\Dataprocessor;

use CS\Theme\Domain\Model\Field;
use CS\Theme\Domain\Model\Generalinformation;
use CS\Theme\Domain\Model\Socialmedia;
use CS\Theme\Domain\Repository\FieldRepository;
use CS\Theme\Domain\Repository\GeneralinformationRepository;
use CS\Theme\Domain\Repository\SocialmediaRepository;
use TYPO3\CMS\Frontend\ContentObject\ContentObjectRenderer;
use TYPO3\CMS\Frontend\ContentObject\DataProcessorInterface;

/**
 * Class MyProcessor.
 */
class ImprintData implements DataProcessorInterface
{
    /**
     * @return array
     *
     * @throws \TYPO3\CMS\Extbase\Persistence\Exception\IllegalObjectTypeException
     * @throws \TYPO3\CMS\Extbase\Persistence\Exception\UnknownObjectException
     */
    public function process(
        ContentObjectRenderer $cObj,
        array $contentObjectConfiguration,
        array $processorConfiguration,
        array $processedData
    ) {
        /** @var \TYPO3\CMS\Extbase\Object\ObjectManager $objectManager */
        $objectManager = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('TYPO3\\CMS\\Extbase\\Object\\ObjectManager');

        $return = [];

        // Den Vorschlaghammer instanzieren / aus der Kiste kramen
        $persistenceManager = $objectManager->get('TYPO3\\CMS\\Extbase\\Persistence\\Generic\\PersistenceManager');

        /** @var GeneralinformationRepository $generalInformationRepository */
        $generalInformationRepository = $objectManager->get('CS\\Theme\\Domain\\Repository\\GeneralinformationRepository');

        /** @var FieldRepository $fieldRepository */
        $fieldRepository = $objectManager->get('CS\\Theme\\Domain\\Repository\\FieldRepository');
        /** @var SocialmediaRepository $socialmediaRepository */
        $socialmediaRepository = $objectManager->get('CS\\Theme\\Domain\\Repository\\SocialmediaRepository');

        /** @var Generalinformation $generalInformation */
        $generalInformation = $generalInformationRepository->findByUid(1);

        if ($generalInformation instanceof Generalinformation) {
            $return['companyname'] = $generalInformation->getCompanyname();

            if ($generalInformation->getLogo()) {
                $return['logo'] = $generalInformation->getLogo()->getOriginalResource()->getPublicUrl();
            }
            $return['city'] = $generalInformation->getCity();
            $return['zip'] = $generalInformation->getZip();
            $return['address'] = $generalInformation->getAddress();
            $return['email'] = $generalInformation->getEmail();
            $return['fax'] = $generalInformation->getFax();
            $return['openingtimes'] = $generalInformation->getOpentimes();
            $return['phone'] = $generalInformation->getPhone();
            $return['ceo'] = $generalInformation->getCeo();
            $return['registrationnumber'] = $generalInformation->getRegistrationnumber();
            $return['registrationcourt'] = $generalInformation->getRegistrationcourt();
            $return['taxid'] = $generalInformation->getTaxid();

            /** @var Field $field */
            foreach ($generalInformation->getFields() as $field) {
                $variablename = strtolower(str_replace(
                    ' ',
                    '',
                    preg_replace('/[^A-Za-z0-9 ]/', '', $field->getTitle())
                ));
                $field->setVariablename($variablename);
                $fieldRepository->update($field);
                $persistenceManager->persistAll();

                if (0 === $field->getType()) {
                    $return[$variablename] = $field->getValue();
                } elseif ($field->getImage()) {
                    $return[$variablename] = $field->getImage();
                }
            }

            /** @var Socialmedia $field */
            foreach ($generalInformation->getSocialmedias() as $field) {
                $variablename = strtolower(str_replace(
                    ' ',
                    '',
                    preg_replace('/[^A-Za-z0-9 ]/', '', $field->getTitle())
                ));
                $socialmediaRepository->update($field);
                $persistenceManager->persistAll();

                $return['socialmedia'][$variablename] = [
                    'title' => $field->getTitle(),
                    'profileurl' => $field->getProfileurl(),
                    'username' => $field->getUsername(),
                ];

                if ($field->getProfilepicture()) {
                    $return['socialmedia'][$variablename]['title'] = $field->getProfilepicture()->getOriginalResource()->getPublicUrl();
                }
            }

            $processedData['companydata'] = $return;
        } else {
            $processedData['companydata'] = 'Es wurden noch keine globalen Firmeninformationen angelegt.';
        }

        return $processedData;
    }
}

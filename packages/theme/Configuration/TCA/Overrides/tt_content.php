<?php

defined('TYPO3_MODE') or die();

$fields = [
    'headertype' => [
        'exclude' => 1,
        'label' => 'LLL:EXT:theme/Resources/Private/Language/locallang.xlf:ttcontent.headertype',
        'config' => [
            'type' => 'select',
            'renderType' => 'selectSingle',
            'items' => [
                ['div', 'div'],
                //['h1', 'h1'],
                ['h2', 'h2'],
                ['h3', 'h3'],
                ['h4', 'h4'],
                ['h5', 'h5'],
                ['h6', 'h6'],
            ],
        ],
    ],
    'subheadertype' => [
        'exclude' => 1,
        'label' => 'LLL:EXT:theme/Resources/Private/Language/locallang.xlf:ttcontent.subheadertype',
        'config' => [
            'type' => 'select',
            'renderType' => 'selectSingle',
            'items' => [
                ['div', 'div'],
                //['h1', 'h1'],
                ['h2', 'h2'],
                ['h3', 'h3'],
                ['h4', 'h4'],
                ['h5', 'h5'],
                ['h6', 'h6'],
                ['p', 'p'],
            ],
        ],
    ],
];

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTCAcolumns('tt_content', $fields);
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addFieldsToPalette('tt_content', 'header', 'headertype,subheadertype', 'after:header_layout');
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addFieldsToPalette('tt_content', 'headers', 'headertype,subheadertype', 'after:header_layout');
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addFieldsToPalette('tt_content', 'header', '--linebreak--,subheader;LLL:EXT:cms/locallang_ttc.xlf:subheader_formlabel', 'after:header');

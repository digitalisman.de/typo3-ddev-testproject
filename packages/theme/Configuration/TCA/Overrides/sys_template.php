<?php

$_EXTKEY = 'theme';

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile(
    $_EXTKEY,
    'Configuration/TypoScript',
    'Fluid Typo3 Theme'
);

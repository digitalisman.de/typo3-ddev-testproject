<?php

return [
    'ctrl' => [
        'title' => 'LLL:EXT:theme/Resources/Private/Language/locallang_db.xlf:tx_theme_domain_model_generalinformation',
        'label' => 'companyname',
        'tstamp' => 'tstamp',
        'crdate' => 'crdate',
        'cruser_id' => 'cruser_id',
        'versioningWS' => true,
        'languageField' => 'sys_language_uid',
        'transOrigPointerField' => 'l10n_parent',
        'transOrigDiffSourceField' => 'l10n_diffsource',
        'delete' => 'deleted',
        'enablecolumns' => [
            'disabled' => 'hidden',
            'starttime' => 'starttime',
            'endtime' => 'endtime',
        ],
        'searchFields' => 'companyname,logo,address,zip,city,email,phone,opentimes,fax,fields,socialmedias',
        'iconfile' => 'EXT:theme/Resources/Public/Icons/tx_theme_domain_model_generalinformation.gif',
    ],
    'interface' => [
        'showRecordFieldList' => 'sys_language_uid, l10n_parent, l10n_diffsource, hidden, companyname, logo, address, zip, city, email, phone, fax, opentimes, ceo, taxid, registrationnumber, registrationcourt, fields, socialmedias',
    ],
    'types' => [
        '1' => [
            'showitem' => 'sys_language_uid, l10n_parent, l10n_diffsource, hidden, companyname, logo, address, zip, city, email, phone, fax, opentimes, ceo, taxid, registrationnumber, registrationcourt, fields, socialmedias, --div--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:tabs.access, starttime, endtime',
        ],
    ],
    'columns' => [
        'sys_language_uid' => [
            'exclude' => true,
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.language',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'special' => 'languages',
                'items' => [
                    [
                        'LLL:EXT:lang/locallang_general.xlf:LGL.allLanguages',
                        -1,
                        'flags-multiple',
                    ],
                ],
                'default' => 0,
            ],
        ],
        'l10n_parent' => [
            'displayCond' => 'FIELD:sys_language_uid:>:0',
            'exclude' => true,
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.l18n_parent',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'default' => 0,
                'items' => [
                    ['', 0],
                ],
                'foreign_table' => 'tx_theme_domain_model_generalinformation',
                'foreign_table_where' => 'AND tx_theme_domain_model_generalinformation.pid=###CURRENT_PID### AND tx_theme_domain_model_generalinformation.sys_language_uid IN (-1,0)',
            ],
        ],
        'l10n_diffsource' => [
            'config' => [
                'type' => 'passthrough',
            ],
        ],
        't3ver_label' => [
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.versionLabel',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'max' => 255,
            ],
        ],
        'hidden' => [
            'exclude' => true,
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.hidden',
            'config' => [
                'type' => 'check',
                'items' => [
                    '1' => [
                        '0' => 'LLL:EXT:lang/Resources/Private/Language/locallang_core.xlf:labels.enabled',
                    ],
                ],
            ],
        ],
        'starttime' => [
            'exclude' => true,
            'behaviour' => [
                'allowLanguageSynchronization' => true,
            ],
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.starttime',
            'config' => [
                'type' => 'input',
                'renderType' => 'inputDateTime',
                'size' => 13,
                'eval' => 'datetime',
                'default' => 0,
            ],
        ],
        'endtime' => [
            'exclude' => true,
            'behaviour' => [
                'allowLanguageSynchronization' => true,
            ],
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.endtime',
            'config' => [
                'type' => 'input',
                'renderType' => 'inputDateTime',
                'size' => 13,
                'eval' => 'datetime',
                'default' => 0,
                'range' => [
                    'upper' => mktime(0, 0, 0, 1, 1, 2038),
                ],
            ],
        ],

        'companyname' => [
            'exclude' => true,
            'label' => 'LLL:EXT:theme/Resources/Private/Language/locallang_db.xlf:tx_theme_domain_model_generalinformation.companyname',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim',
            ],
        ],
        'logo' => [
            'exclude' => true,
            'label' => 'LLL:EXT:theme/Resources/Private/Language/locallang_db.xlf:tx_theme_domain_model_generalinformation.logo',
            'config' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::getFileFieldTCAConfig(
                'logo',
                [
                    'appearance' => [
                        'createNewRelationLinkTitle' => 'LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:images.addFileReference',
                    ],
                    'overrideChildTca' => [
                        'types' => [
                            'foreign_types' => [
                                '0' => [
                                    'showitem' => '
			                --palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
			                --palette--;;filePalette',
                                ],
                                \TYPO3\CMS\Core\Resource\File::FILETYPE_TEXT => [
                                    'showitem' => '
			                --palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
			                --palette--;;filePalette',
                                ],
                                \TYPO3\CMS\Core\Resource\File::FILETYPE_IMAGE => [
                                    'showitem' => '
			                --palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
			                --palette--;;filePalette',
                                ],
                                \TYPO3\CMS\Core\Resource\File::FILETYPE_AUDIO => [
                                    'showitem' => '
			                --palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
			                --palette--;;filePalette',
                                ],
                                \TYPO3\CMS\Core\Resource\File::FILETYPE_VIDEO => [
                                    'showitem' => '
			                --palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
			                --palette--;;filePalette',
                                ],
                                \TYPO3\CMS\Core\Resource\File::FILETYPE_APPLICATION => [
                                    'showitem' => '
			                --palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
			                --palette--;;filePalette',
                                ],
                            ],
                        ],
                    ],
                    'maxitems' => 1,
                ],
                $GLOBALS['TYPO3_CONF_VARS']['GFX']['imagefile_ext']
            ),
        ],
        'address' => [
            'exclude' => true,
            'label' => 'LLL:EXT:theme/Resources/Private/Language/locallang_db.xlf:tx_theme_domain_model_generalinformation.address',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim',
            ],
        ],
        'zip' => [
            'exclude' => true,
            'label' => 'LLL:EXT:theme/Resources/Private/Language/locallang_db.xlf:tx_theme_domain_model_generalinformation.zip',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim',
            ],
        ],
        'city' => [
            'exclude' => true,
            'label' => 'LLL:EXT:theme/Resources/Private/Language/locallang_db.xlf:tx_theme_domain_model_generalinformation.city',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim',
            ],
        ],
        'email' => [
            'exclude' => true,
            'label' => 'LLL:EXT:theme/Resources/Private/Language/locallang_db.xlf:tx_theme_domain_model_generalinformation.email',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim',
            ],
        ],
        'phone' => [
            'exclude' => true,
            'label' => 'LLL:EXT:theme/Resources/Private/Language/locallang_db.xlf:tx_theme_domain_model_generalinformation.phone',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim',
            ],
        ],
        'opentimes' => [
            'exclude' => true,
            'label' => 'LLL:EXT:theme/Resources/Private/Language/locallang_db.xlf:tx_theme_domain_model_generalinformation.opentimes',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim',
            ],
        ],
        /*
        'opentimes' => [
            'exclude' => true,
            'label' => 'LLL:EXT:theme/Resources/Private/Language/locallang_db.xlf:tx_theme_domain_model_generalinformation.opentimes',
            'config' => [
                'type' => 'text',
                'enableRichtext' => true,
                'richtextConfiguration' => 'default',
                'fieldControl' => [
                    'fullScreenRichtext' => [
                        'disabled' => false,
                    ],
                ],
                'cols' => 40,
                'rows' => 15,
                'eval' => 'trim',
            ],
        ],
        */
        'fax' => [
            'exclude' => true,
            'label' => 'LLL:EXT:theme/Resources/Private/Language/locallang_db.xlf:tx_theme_domain_model_generalinformation.fax',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim',
            ],
        ],
        'ceo' => [
            'exclude' => true,
            'label' => 'LLL:EXT:theme/Resources/Private/Language/locallang_db.xlf:tx_theme_domain_model_generalinformation.ceo',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim',
            ],
        ],
        'taxid' => [
            'exclude' => true,
            'label' => 'LLL:EXT:theme/Resources/Private/Language/locallang_db.xlf:tx_theme_domain_model_generalinformation.taxid',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim',
            ],
        ],
        'registrationnumber' => [
            'exclude' => true,
            'label' => 'LLL:EXT:theme/Resources/Private/Language/locallang_db.xlf:tx_theme_domain_model_generalinformation.registrationnumber',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim',
            ],
        ],
        'registrationcourt' => [
            'exclude' => true,
            'label' => 'LLL:EXT:theme/Resources/Private/Language/locallang_db.xlf:tx_theme_domain_model_generalinformation.registrationcourt',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim',
            ],
        ],
        'fields' => [
            'exclude' => true,
            'label' => 'LLL:EXT:theme/Resources/Private/Language/locallang_db.xlf:tx_theme_domain_model_generalinformation.fields',
            'config' => [
                'type' => 'inline',
                'foreign_table' => 'tx_theme_domain_model_field',
                'foreign_field' => 'generalinformation',
                'foreign_sortby' => 'sorting',
                'maxitems' => 9999,
                'appearance' => [
                    'collapseAll' => 1,
                    'levelLinksPosition' => 'top',
                    'showSynchronizationLink' => 1,
                    'showPossibleLocalizationRecords' => 1,
                    'useSortable' => 1,
                    'showAllLocalizationLink' => 1,
                ],
            ],
        ],
        'socialmedias' => [
            'exclude' => true,
            'label' => 'LLL:EXT:theme/Resources/Private/Language/locallang_db.xlf:tx_theme_domain_model_generalinformation.socialmedias',
            'config' => [
                'type' => 'inline',
                'foreign_table' => 'tx_theme_domain_model_socialmedia',
                'foreign_field' => 'generalinformation',
                'foreign_sortby' => 'sorting',
                'maxitems' => 9999,
                'appearance' => [
                    'collapseAll' => 1,
                    'levelLinksPosition' => 'top',
                    'showSynchronizationLink' => 1,
                    'showPossibleLocalizationRecords' => 1,
                    'useSortable' => 1,
                    'showAllLocalizationLink' => 1,
                ],
            ],
        ],
    ],
];

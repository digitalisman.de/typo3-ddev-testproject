<?php

if (! defined('TYPO3_MODE')) {
    die('Access denied.');
}

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig(
    '<INCLUDE_TYPOSCRIPT: source="DIR:EXT:theme/Configuration/PageTS">'
);

/*
 * Add default TypoScript Config für all users
 */
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addUserTSConfig('
	options {
		pageTree.showPageIdWithTitle = 1
	}
');

//\FluidTYPO3\Flux\Core::registerProviderExtensionKey('Cs.Theme', 'Page');
//\FluidTYPO3\Flux\Core::registerProviderExtensionKey('Cs.Theme', 'Content');

/*
 *  Preset for ckEditor
 */
$GLOBALS['TYPO3_CONF_VARS']['RTE']['Presets']['theme'] = 'EXT:theme/Configuration/CKEditor/Theme.yaml';

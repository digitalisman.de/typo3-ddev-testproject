$(document).ready(function () {
	// Testimonial Slider
	
	var owl = $('.testimonial .owl-carousel');
	owl.owlCarousel({
	    loop:true,
	    margin:10,
	    nav:false,
	    items:1
	});
	$('.testimonial-nav .next').click(function() {
	    owl.trigger('next.owl.carousel');
	});
	$('.testimonial-nav .prev').click(function() {
	    //owl.trigger('prev.owl.carousel', [300]);
	    owl.trigger('prev.owl.carousel');
	});

	// Image Slider
	
	var imageSlider = $('.image-slider');
	imageSlider.owlCarousel({
	    loop:true,
	    margin:0,
	    nav:true,
	    items:1,
	    dots:true,
	    dotsEach:true,
	    autoplay:true,
	    autoplayHoverPause:true,
	    smartSpeed:500,
	    autoplayTimeout:8000,
	    navText:['<i class="far fa-arrow-alt-circle-left prev"></i>','<i class="far fa-arrow-alt-circle-right next"></i>'] 
	});

	// Leistungen Slider
	var leistungsOwl = $('.leistungen .owl-carousel');
	leistungsOwl.owlCarousel({
	    loop:true,
	    margin:10,
	    nav:false,
	    responsive:{
	        0:{
	            items:1
	        },
	        768:{
	            items:2
	        },
	        992:{
	            items:3
	        },
	    }
	});

	$('.navi-leistungen .next').click(function() {
	    leistungsOwl.trigger('next.owl.carousel');
	});
	$('.navi-leistungen .prev').click(function() {
	    leistungsOwl.trigger('prev.owl.carousel');
	});

	// Case Studies Slider
	var casestudies = $('.products .owl-carousel');
	casestudies.owlCarousel({
	    loop:true,
	    margin:10,
	    nav:false,
	    responsive:{
	        0:{
	            items:1
	        },
	        768:{
	            items:2
	        },
	        992:{
	            items:3
	        },
	    }
	});

	$('.casestudie-nav .next').click(function() {
	    casestudies.trigger('next.owl.carousel');
	});
	$('.casestudie-nav .prev').click(function() {
	    casestudies.trigger('prev.owl.carousel');
	});
	
	// News Slider
	var newsOwl = $('.news .owl-carousel');
	newsOwl.owlCarousel({
	    loop:true,
	    margin:10,
	    nav:false,
	    responsive:{
	        0:{
	            items:1
	        },
	        768:{
	            items:2
	        },
	        992:{
	            items:3
	        },
	    }
	});
	$('.navi-news .next').click(function() {
	    newsOwl.trigger('next.owl.carousel');
	});
	$('.navi-news .prev').click(function() {
	    newsOwl.trigger('prev.owl.carousel');
	});

	// Partner Logo Slider
	var partnerOwl = $('.partner-logos .owl-carousel');
	partnerOwl.owlCarousel({
	    loop:true,
	    margin:10,
	    nav:false,
	    responsive:{
	        0:{
	            items:1
	        },
	        768:{
	            items:2
	        },
	        992:{
	            items:5
	        },
	    }
	});
	$('.btn-group .btn-next').click(function() {
	    partnerOwl.trigger('next.owl.carousel');
	});
	$('.btn-group .btn-prev').click(function() {
	    partnerOwl.trigger('prev.owl.carousel');
	});

	// Produkt Detail Slider
	var OwlSlider = $('#owl-slider');
	OwlSlider.owlCarousel({
	    loop:true,
	    margin:0,
	    nav:false,
	    items:1,
	    dots:true,
	    dotsEach:true,
	    autoplay:true,
	    autoplayHoverPause:true,
	    smartSpeed:500,
	    autoplayTimeout:8000
	});

	// Product Detail Form einblenden
	$('.inquiryform #productInquiry').click(function() {
	    if ( $('.product-inquiry-form').hasClass('open') ) {
		    $('.product-inquiry-form').fadeOut().removeClass('open');
	    } else {
		    $('.product-inquiry-form').fadeIn().addClass('open');
	    }
	    
	    
	    //$(this).fadeOut();
	    
	});

	// Typo3 Alerts automatisch ausblenden nach 5 Sekunden
	function closeAlerts() {
	  $('.typo3-messages').fadeOut(500);
	}

	if ($('.typo3-messages').length == 1) {
    	setTimeout(closeAlerts, 5000);
	}
});
// Toggle all Accordions by default on Desktop

$(document).ready(function() {
	
	function collapseAccordion() {
		if ($(window).width() >= 960 && $("#property-accordion").length == 1) {
			$('#property-accordion .collapse:not(".show")').collapse('show');
		} else {
			$('#property-accordion .collapse.show').collapse('hide');
		}
	}
	
	collapseAccordion();
	
	$(window).resize(function() {
		collapseAccordion();
	});
	/*
	$('.bereiche .btn').click(function(event) {
		event.preventDefault();
		var test = $(this).attr('href');
		
		console.log(test);
		
		$([document.documentElement, document.body]).animate({
		  scrollTop: $(this).attr('href').offset().top - 120
		}, 500);
		
	});
	*/
});

$(document).ready(function(){
	$('.bereiche .btn').click(function(event){
		event.preventDefault();
		$("html, body").animate({
        	scrollTop: $($(this).attr('href')).offset().top - 0
    	}, 500);
    });
    
    // Swiper Default
    var mySwiper = new Swiper ('.swiper-slider', {
		direction: 'horizontal',
	    loop: false,
	    effect: 'fade',
		autoplay: {
			delay: 5000,
		},
		fadeEffect: {
			crossFade: true
		},
		pagination: {
			el: '.swiper-pagination',
			type: 'progressbar',
		},
		navigation: {
			nextEl: 'i.next',
			prevEl: 'i.prev',
    	},
    });
    
    // Swiper Default
    var mySlider = new Swiper ('#slider', {
		direction: 'horizontal',
	    loop: false,
	    effect: 'fade',
		autoplay: {
			delay: 8000,
		},
		fadeEffect: {
			crossFade: true
		},
		pagination: {
			el: '.swiper-pagination',
			type: 'progressbar',
		},
		navigation: {
			nextEl: 'i.next',
			prevEl: 'i.prev',
    	},
    });
    
    // Swiper Default
    var mySwiperCarousel = new Swiper ('.swiper-carousel', {
		direction: 'horizontal',
	    loop: true,
	    //effect: 'fade',
	    slidesPerView: 3,
		spaceBetween: 30,
		autoplay: {
			delay: 8000,
		},
		breakpoints: {
			1024: {
				slidesPerView: 3,
				spaceBetween: 30,
			},	
			992: {
				slidesPerView: 2,
				spaceBetween: 30,
			},
			768: {
        	  slidesPerView: 1,
        	  spaceBetween: 10,
        	}
		},
		//fadeEffect: {
		//	crossFade: true
		//},
		//pagination: {
		//	el: '.swiper-pagination',
		//	type: 'progressbar',
		//},
		navigation: {
			nextEl: 'i.next',
			prevEl: 'i.prev',
    	},
    });
    
    // Responsive FontSize
    Textblock([
	 {
	  target: ".call-to-action h2",
	  minFontSize: 1.3,
	  maxFontSize: 2,
	  units:"rem",
	 },
	 {
	  target: ".content h1",
	  minFontSize: 1.4,
	  maxFontSize: 2.5,
	  units:"rem",
	  container: "self",
	  minVariableGrade: 300,
	  maxVariableGrade: 600
	 },
	 {
	  target: ".jumbotron h1",
	  minFontSize: 1.5,
	  maxFontSize: 2.7,
	  units:"rem",
	  container: "self",
	  minVariableGrade: 300,
	  maxVariableGrade: 600
	 },
	 {
	  target: ".headline .h2",
	  minFontSize: 1.5,
	  maxFontSize: 2.0,
	  units:"rem",
	  container: "self",
	  minVariableGrade: 300,
	  maxVariableGrade: 600
	 },
	 {
	  target: ".jumbotron p",
	  minFontSize: 1.5,
	  maxFontSize: 2.7,
	  units:"rem",
	  container: "self",
	  minVariableGrade: 300,
	  maxVariableGrade: 600
	 },
	 {
	  target: ".related--content h4",
	  minFontSize: 1.0,
	  maxFontSize: 1.8,
	  units:"rem",
	  //container: "self",
	  minVariableGrade: 300,
	  maxVariableGrade: 600
	 },
	 {
	  target: ".card h3",
	  minFontSize: 1.0,
	  maxFontSize: 1.8,
	  units:"rem",
	  //container: "self",
	  minVariableGrade: 300,
	  maxVariableGrade: 600
	 },
	 {
	  target: ".contentwrap h2",
	  minFontSize: 1.2,
	  maxFontSize: 2.0,
	  units:"rem",
	  container: "self",
	  minVariableGrade: 300,
	  maxVariableGrade: 600
	 }

	]);
});


<?php
namespace Deployer;

require 'vendor/deployer/deployer/recipe/typo3.php';

//require_once(__DIR__ . '/vendor/sourcebroker/deployer-loader/autoload.php');
//new \SourceBroker\DeployerExtendedTypo3\Loader();

// Project name
set('application', 'my_deployer_test');
set('http_user','web');
// Project repository
set('repository', 'git@gitlab.com:digitalisman.de/typo3-ddev-testproject.git');
set('default_stage', 'staging');
// [Optional] Allocate tty for git clone. Default value is false.
set('git_tty', false);
# DocumentRoot / WebRoot for the TYPO3 installation
set('typo3_webroot', 'public');
set('web_path', 'public/');
// Shared files/dirs between deploys
//add('shared_files', ['composer.json','composer.lock']);
//add('shared_dirs', ['packages','config','packages']);

# Shared directories
//add('shared_dirs', [
//    'public',
//    'packages',
//    'config',
//    '{{typo3_webroot}}/fileadmin',
//    '{{typo3_webroot}}/typo3temp',
//    '{{typo3_webroot}}/uploads',
//]);

/**
 * Writeable directories
 */
set('writable_dirs', [
    '{{typo3_webroot}}/fileadmin',
    '{{typo3_webroot}}/typo3temp',
    '{{typo3_webroot}}/typo3conf',
    '{{typo3_webroot}}/uploads',
    '/var'
]);

// Writable dirs by web server
//add('writable_dirs', ['var']);
set('allow_anonymous_stats', false);

// Hosts

host('lomefe.han-solo.net')
    #->hostname('lomefe.han-solo.net')
    ->stage('staging')
    ->user('admin')
    ->set('branch', 'master')
    ->port(22)
    ->configFile('~/.ssh/config')
    ->identityFile('~/.ssh/id_rsa')
    ->forwardAgent(true)
    ->multiplexing(true)
    ->set('deploy_path', '/usr/local/www/apache24/noexec/my_deployer_test');

// [Optional] if deploy fails automatically unlock.
after('deploy:failed', 'deploy:unlock');

after('deploy', 'success');

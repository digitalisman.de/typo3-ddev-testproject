<?php

declare(strict_types=1);

use PhpCsFixer\Fixer\Phpdoc\PhpdocAlignFixer;
use Symfony\Component\DependencyInjection\Loader\Configurator\ContainerConfigurator;
use Symplify\EasyCodingStandard\Configuration\Option;
use Symplify\EasyCodingStandard\ValueObject\Set\SetList;

return static function (ContainerConfigurator $containerConfigurator): void {
    $parameters = $containerConfigurator->parameters();
    $parameters->set(Option::PATHS, [
        __DIR__ . '/packages',
        __DIR__ . '/public/typo3conf/AdditionalConfiguration.php',
    ]);

    $parameters->set(Option::SETS, [
        SetList::CLEAN_CODE,
        SetList::COMMENTS,
        SetList::COMMON,
        SetList::CONTROL_STRUCTURES,
        SetList::DEAD_CODE,
        SetList::DOCBLOCK,
        SetList::NAMESPACES,
        SetList::SPACES,
        SetList::CLEAN_CODE,
        SetList::SYMFONY,
        SetList::SYMFONY_RISKY,
        SetList::DEAD_CODE,
        SetList::PSR_12,
        SetList::PHP_71,
    ]);

    $parameters->set(Option::SKIP, [PhpdocAlignFixer::class => null]);
};

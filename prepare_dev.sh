#!/usr/bin/env bash

#color variabes
RED='\033[38;5;1m'
GREEN='\033[38;5;10m'
YELLOW='\033[38;5;11m'
NC='\033[38;5;0m' # No Color

printf "${GREEN}Wie ist der Projektname? (keine sonderzeichen, außer -):${NC} "
read projectname

# switch to packages folder
cd packages

# clone all dauskonzept* extensions
git clone git@gitlab.com:digitalisman/theme.git && sleep 1
git clone git@gitlab.com:digitalisman/themeoverride.git && sleep 1

# back to root folder
cd ..

# Move Pipline & EasyCodeStandard
cp INSTALL/prepare/rootfiles/* .
cp INSTALL/prepare/rootfiles/.* .

git add * && sleep 2
git add .gitlab-ci.yml -f && sleep 2

# Install all dependencies
composer install && sleep 2

ddev config --project-type=typo3 --docroot=public --project-name=$projectname && sleep 2
ddev start && sleep 2

# add default fileadmin folders
mkdir -p public/fileadmin/Bilder/{Header,Inhaltsbilder,Logos,Mitarbeiter,News,Teaser}
mkdir -p public/fileadmin/Dokumente/
mkdir -p public/fileadmin/Videos/

# Setup TYPO3 (creates LocalConfiguration.php)
ddev exec ./vendor/bin/typo3cms install:setup --no-interaction --admin-user-name digitalisman --admin-password temp#123 && sleep 2

# import default-database from file
ddev import-db --src=INSTALL/dump.sql && sleep 2

# update databaseschema in case of differences
ddev exec vendor/bin/typo3cms database:updateschema && sleep 2
ddev exec vendor/bin/typo3cms database:updateschema destructive && sleep 2
ddev exec vendor/bin/typo3cms database:updateschema destructive && sleep 2

cp public/typo3/sysext/install/Resources/Private/FolderStructureTemplateFiles/root-htaccess public/.htaccess
git add public/.htaccess -f && sleep 2
git add public/typo3conf/AdditionalConfiguration.php -f && sleep 2

# Add compression string on top of file
sed -i '' "/### Begin: Compression ###/ a\\
<FilesMatch \"\\\.js\\\.gzip$\">\\
\ \ \ \ AddType \"text/javascript\" .gzip\\
</FilesMatch>\\
<FilesMatch \"\\\.css\\\.gzip$\">\\
\ \ \ \ AddType \"text/css\" .gzip\\
</FilesMatch>\\
AddEncoding gzip .gzip\\
" public/.htaccess

# Add Context to root .htaccess
echo "#SetEnvIf Host www\.muster\.de$ TYPO3_CONTEXT=Production
SetEnvIf Host (.*)\.digitalisman\.de$ TYPO3_CONTEXT=Testing
SetEnvIf Host (.*)\.ddev\.site$ TYPO3_CONTEXT=Development" >> public/.htaccess && sleep 1

# move site-config in place
mkdir -p ./config/sites/$projectname && sleep 1
cp INSTALL/prepare/siteconfig/default.config.yaml config/sites/$projectname/config.yaml && sleep 1
git add config/sites/$projectname/config.yaml && sleep 1

#populate site-config data
printf "${YELLOW}###SITE-CONFIG EINSTELLUNGEN###\n"
printf "${GREEN}Wie ist die künftige Live-Domain?(${RED}ohne https://!): ${NC}"
read baseurl
sed -i '' "s/###BASE_URL###/https:\/\/$baseurl/g" ./config/sites/$projectname/config.yaml

printf "${GREEN}Wie ist die Dev-Domain?: ${NC}"
read -p "($projectname.ddev.site)" baseurldev
baseurldev=${baseurldev:-$projectname.ddev.site}
sed -i '' "s/###BASE_URL_DEVELOPMENT###/https:\/\/$baseurldev/g" ./config/sites/$projectname/config.yaml

printf "${GREEN}Wie ist die Testing-Domain?: ${NC}"
read -p "($projectname.digitalisman.de)" baseurltest
baseurltest=${baseurltest:-$projectname.digitalisman.de}
sed -i '' "s/###BASE_URL_TESTING###/https:\/\/$baseurltest/g" ./config/sites/$projectname/config.yaml

printf "${GREEN} SITE-CONFIG ENDE ✅\n\n"

# flush all caches
ddev exec TYPO3_CONTEXT=Development ./vendor/bin/typo3cms cache:flush

# print useful project information
ddev describe && sleep 1
printf "${GREEN}Website ist erreichbar unter: https://$baseurldev ${NC} "
echo "Installation abgeschlossen 🥰"

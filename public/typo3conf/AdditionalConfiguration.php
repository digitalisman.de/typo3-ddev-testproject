<?php

\TYPO3\CMS\Core\Utility\ArrayUtility::mergeRecursiveWithOverrule(
    $GLOBALS['TYPO3_CONF_VARS'],
    [
        'BE' => [
            'installToolPassword' => '$argon2i$v=19$m=16384,t=16,p=2$eWhTRTd5Nm4vQkRvSUZ4UA$Ydyqg+KfMOH2T/XOXQA1vCcVnzzCv1ErbiwllmMhYes',
        ],
        'DB' => [
            'Connections' => [
                'Default' => [
                    // hier die Live Datenbank daten eintragen
                    'dbname' => 'usr_pxxxxxx_1',
                    'host' => 'mydbserver.com',
                    'password' => '',
                    'port' => '3306',
                    'user' => 'pxxxxxx',
                ],
            ],
        ],
        'EXTENSIONS' => [
            'backend' => [
                'backendFavicon' => '',
                'backendLogo' => 'EXT:theme/Resources/Public/Icons/Favicon/favicon-32x32.png',
                'loginBackgroundImage' => 'EXT:theme/Resources/Backend/Images/login-background.jpg',
                'loginFootnote' => '© 2020 Digitalisman | www.digitalisman.de',
                'loginHighlightColor' => '#75b729',
                'loginLogo' => 'EXT:theme/Resources/Backend/Images/login-logo.svg',
            ],
            'ke_search_premium' => [
                'boostKeywordsFactor' => '5',
                'enableAutocomplete' => '1',
                'enableBoostKeywords' => '0',
                'enableCustomRanking' => '0',
                'enableDoYouMean' => '1',
                'enableSynonyms' => '1',
            ],
            'news' => [
                'advancedMediaPreview' => '1',
                'archiveDate' => 'date',
                'categoryBeGroupTceFormsRestriction' => '0',
                'categoryRestriction' => 'none',
                'contentElementPreview' => '1',
                'contentElementRelation' => '1',
                'dateTimeNotRequired' => '0',
                'hidePageTreeForAdministrationModule' => '0',
                'manualSorting' => '0',
                'mediaPreview' => '0',
                'prependAtCopy' => '1',
                'resourceFolderImporter' => '/news_import',
                'rteForTeaser' => '0',
                'showAdministrationModule' => '1',
                'showImporter' => '0',
                'storageUidImporter' => '1',
                'tagPid' => '1',
            ],
            'scheduler' => [
                'maxLifetime' => '1440',
                'showSampleTasks' => '1',
            ],
        ],
        'SYS' => [
            'sitename' => 'Digitalisman',
            'systemMaintainers' => [
                1,
                3,
                5,
            ],
        ],
    ]
);

if ('true' === getenv('IS_DDEV_PROJECT')) {
    if (\TYPO3\CMS\Core\Core\Environment::getContext()->isDevelopment()) {
        \TYPO3\CMS\Core\Utility\ArrayUtility::mergeRecursiveWithOverrule(
            $GLOBALS['TYPO3_CONF_VARS'],
            [
                'FE' => [
                    'debug' => true,
                ],
                'DB' => [
                    'Connections' => [
                        'Default' => [
                            'dbname' => 'db',
                            'host' => 'db',
                            'password' => 'db',
                            'port' => '3306',
                            'user' => 'db',
                        ],
                    ],
                ],
                'GFX' => [
                    'processor' => 'ImageMagick',
                    'processor_path' => '/usr/bin/',
                    'processor_path_lzw' => '/usr/bin/',
                ],
                'MAIL' => [
                    'transport' => 'smtp',
                    'transport_smtp_server' => 'localhost:1025',
                ],
                'SYS' => [
                    'trustedHostsPattern' => '.*.*',
                    'devIPmask' => '*',
                    'displayErrors' => 1,
                ],
            ]
        );
    } else {
        throw new \TYPO3\CMS\Core\Exception('DDEV, but not Development-Context!!!');
    }
} else {
    if (\TYPO3\CMS\Core\Core\Environment::getContext()->isDevelopment()) {
        throw new \TYPO3\CMS\Core\Exception('isDevelopment = true! Without DDEV, we should be in Testing mode!');
    }

    if (\TYPO3\CMS\Core\Core\Environment::getContext()->isTesting()) {
        \TYPO3\CMS\Core\Utility\ArrayUtility::mergeRecursiveWithOverrule(
            $GLOBALS['TYPO3_CONF_VARS'],
            [
                'BE' => [
                    'debug' => true,
                ],
                'FE' => [
                    'debug' => true,
                ],
                'SYS' => [
                    'trustedHostsPattern' => '.*.*',
                    'devIPmask' => '*',
                    'displayErrors' => 1,
                ],
            ]
        );
    }

    if (\TYPO3\CMS\Core\Core\Environment::getContext()->isProduction()) {
        \TYPO3\CMS\Core\Utility\ArrayUtility::mergeRecursiveWithOverrule(
            $GLOBALS['TYPO3_CONF_VARS'],
            [
                'FE' => [
                    'debug' => false,
                    'compressionLevel' => 9,
                ],
                'BE' => [
                    'debug' => false,
                    'compressionLevel' => 9,
                ],
                'SYS' => [
                    'displayErrors' => 0,
                    'exceptionalErrors' => 4096,
                    'systemLogLevel' => 2,
                ],
            ]
        );
    }
}

# Kunden Dummy
DDEV Kunden-Package mit allen abhängigkeiten. Dieses Projekt hat immer aktuell zu sein und darf NICHT bearbeitet werden! Für Anpassungen bitte Forken und Merge-Requests erstellen.

## Vorbereitung
im Gitlab das Repo `https://gitlab.com/dauskonzept/dummy-projects/customer-dummy` **forken**.
Bei diesem Repo in die Settings -> Erweitert -> `Remove fork relationship`

## Installation:
1. Lokalen Ordner für das Projekt erstellen und in den Ordner wechseln.
    * Bsp.: `mkdir TestProjekt` & `cd TestProjekt`
2. Repository Klonen `git@gitlab.com:dauskonzept/customer-packages/customer-dummy.git .` **(mit Punkt am Ende)**
3. In der `composer.json` die Felder `name` und `description` anpassen, dann prüfen ob alle Extensions gebraucht werden (siehe Notiz im Terminal).
4. prepare.sh direkt aus dem Projektroot ausführen und den Aufforderungen folgen: `./prepare.sh`

# DDEV Hints
* Daten des aktuellen Projektes anzeigen:
    * `ddev describe`
* Daten eines andere Containers anzeigen:
    * `ddev describe <proj_name>`
* aktuelles Projekt entfernen
    * `ddev delete`
* Container auflisten:
    * `ddev list`
* Container entfernen:
    * `ddev remove <proj_name>`
* ALLE Container herunterfahren
    * `ddev poweroff`

# Typo3 Backend Admin
Benutzer: digitalisman<br>
Passwort: temp#123
